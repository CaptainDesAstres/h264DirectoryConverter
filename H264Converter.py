#!/usr/bin/python3.4
# -*-coding:Utf-8 -*

import sys, os, subprocess, shlex, datetime
from mimetypes import MimeTypes
mimetype = MimeTypes()

def main():
	# récupére les argument de la commande (path et cpu limit):
	arguments = sys.argv
	arg = arguments.pop()
	try:
		limit = int(arg)
		path = arguments.pop()
	except ValueError:
		limit = False
		path = arg
	path = os.path.realpath(path)
	
	# determine si la cible est un fichier ou un dossier et lance la fonction adéquate
	if(os.path.isdir(path)):
		ConvertDir( path+'/', limit )
	elif(os.access(os.path.realpath(path+'/..'), os.W_OK)):
		ConvertFile( path, limit )
	else:
		print('\033[31mImpossible d\'écrire dans le dossier parent\033[0m')



# fonction pour les dossiers:
def ConvertDir( path, limit ):
	print('\033[34mConversion du contenu de '+path+'\033[0m')
	
	# vérifie que le dossier est lisible 
	if(not os.access(path, os.R_OK)):
		print('\033[31mPas de permission de lecture sur le dossier\033[0m')
		return
	# et que l'on a les droit d'écriture
	if(not os.access(path, os.W_OK)):
		print('\033[31mPas de permission d\'ecriture sur le dossier\033[0m')
		return
	
	
	# récupére la liste de son contenu
	content = os.listdir(path)
	
	# trie les fichier et les dossier dans deux tableau
	dirs = []
	files = []
	for txt in content:
		if( os.path.isdir(path+txt) ):
			if( txt != 'h264'):
				dirs.append(path+txt+'/')
		else:
			files.append(path+txt)
	
	
	# traite d'abord les dossier (si il y en as) par appel récursif
	if len(dirs) >0:
		print('Conversion du contenu de '+str(len(dirs))+' sous-dossier(s).')
		for d in dirs:
			ConvertDir( d, limit )
	
	# si il y a des fichers, lance la fonction dédiée pour chaque fichier
	if len(files) >0:
		print('Conversion de '+str(len(files))+' fichier(s).')
		for f in files:
			ConvertFile( f, limit )



# fonction pour les fichier:
def ConvertFile( path, limit ):
	print('\033[32mConversion du fichier '+path+'\033[0m')
	# vérifie que le fichier est lisible et que c'est bien un fichier video
	if(not os.access(path, os.R_OK)):
		print('\033[31mPas de permission de lecture!\033[0m')
		return
	
	# et que c'est bien un fichier video
	kind = mimetype.guess_type( path)[0]
	if kind is not None and not kind.startswith('video'):
		print('\033[31mLe fichier n\'est pas un fichier video\033[0m')
		return
	
	# créer la ligne de commande
	outputDir = os.path.realpath(path+'/..')+'/h264'
	outPath = outputDir+'/'+os.path.basename(path)
	codec = "-c:v h264_nvenc -profile:v high -pixel_format yuv420p -preset default"
	#codec = "-c:v mpeg4 -b:v 6000k"
	
	#codec += " -c:a libmp3lame"
	#codec += " -c:a ac3"
	
	#codec += " -c:s srt"
	#codec += " -c:s ass"
	#codec += " -sn"#erase subtitles
	
	cmd = 'ffmpeg -y -i "'+path+'" -map 0 -c copy '+codec+' "'+outPath+'"'
	#print(cmd)
	
	
	# créer un dossier de sortie
	if not (os.path.exists(outputDir) and os.path.isdir(outputDir)):
		try:
			os.mkdir(outputDir)
		except:
			print('\033[31mErreur: impossible de créer le dossier de sortie: '+outputDir+'\033[0m')
			return
	
	if not os.access(outputDir, os.W_OK):
		print('\033[31mErreur: impossible d\'écrire dans le dossier de sortie: '+outputDir+'\033[0m')
		return
	
	# lance ffmpeg
	start = datetime.datetime.now()
	print('\033[33m'+start.strftime('%H:%M:%S')+': Début de conversion.\033[0m')
	ffmpeg = subprocess.Popen( shlex.split( cmd ),
			stdin=subprocess.PIPE,
			stdout=subprocess.PIPE,
			stderr=subprocess.PIPE )
	
	# limite l'usage du CPU par ffmpeg
	limitator = subprocess.Popen( 
			shlex.split( 
				'cpulimit -p '+str(ffmpeg.pid)+' -l '+str(limit) 
			), 
			stdin=subprocess.PIPE, 
			stdout=subprocess.PIPE, 
			stderr=subprocess.PIPE )
	
	# attend la fin de la conversion
	ffmpeg.communicate()
	limitator.communicate()
	
	end = datetime.datetime.now()
	delta = end - start
	h, delta = divmod(delta.seconds, 3600)
	m, s = divmod(delta, 60)
	print(end.strftime('%H:%M:%S')+': Fin de conversion. (durée: '+
			str(h)+':'+str(m)+':'+str(s)+')\033[0m')
	

main()

